package com.BMIapp2.BMIcalc;

import java.util.Scanner;


public class App 
{
	public static void main(String[] args) {

    	double weight; 
    	double height; 
    	double bmi; 
    	
    	
    	Scanner keyboard = new Scanner(System.in);

    	
    	System.out.println("This program will calculate your body mass index, or BMI.");

    	// Get the user's weight.
    	System.out.println("Enter your weight, in kg: ");
    	weight = keyboard.nextDouble();

    	// Get the user's height.
    	System.out.println("Enter your height, in cm: ");
    	height = keyboard.nextDouble();
    	}
    	public static double calculateBMI (double weight,double height) {
    		
			
    	    return weight / (Math.pow(height/2,2));
    	}
       
    	public static String bmiDescription (double bmi) {
    	    if (bmi < 18.5) {
    	        return "You are underweight.";
    	    } else {
    	        if (bmi > 25) {
    	            return "You are overweight.";
    	        } else {
    	            return "Your weight is optimal.";
    	        }
    	    }
    	 // a height-et és a weight-et aláhúzza
    	    bmi =  calculateBMI(height, weight);

    	    // Display the user's BMI.
    	    System.out.println("Your body mass index (BMI) is " + bmi);

    	    System.out.println(bmiDescription(bmi));
}
}
