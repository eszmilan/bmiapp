package com.BMIapp2.BMIcalc;


import static org.junit.Assert.*;

import org.junit.Test;



public class AppTest {
    

    @Test
    public void calculate_bmi () {
    	assertTrue(App.calculateBMI(73, 250) == 32.9799211859636);
    }

    @Test
    public void bmiDescription_underweight () {
        assertEquals("You are underweight.", App.bmiDescription(16));
    }

    @Test
    public void bmiDescription_overweight () {
        assertEquals("You are overweight.", App.bmiDescription(78));
    }

    @Test
    public void bmiDescription_optimal () {
        assertEquals("Your weight is optimal.", App.bmiDescription(20));
    }}